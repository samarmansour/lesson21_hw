﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Lesson21_ChallengePart
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = @"..\..\car2.xml";
            string arrayPath = @"..\..\carArray2.xml";
            

            Car seat = new Car("Ateca-Cupra","Seat",2021,"Dark-Gray", 1,5);
            Car.SerializeACar(path, seat);

            Car car = new Car(path);

            Car[] company = new Car[5]
            {
                new Car("Aventador s","Lamborgini",2020,"Yellow",2,2),
                new Car("Land Cruiser", "Toyota", 2018, "Gray",5,5),
                new Car("TTRS", "Audi", 2020, "Black",3,8),
                new Car("Chiron", "Bugatti", 2019, "Blue",1,5),
                new Car("Camaro", "Chevrolate", 2021, "Black",5,5),
            };

            //Car.SerializeACar(path,seat);
            //Console.WriteLine(Car.DeserializeACar(path));


            // Car compare function
            Console.WriteLine(seat.CarComare(path)); //return true
            //Array functions
            
            Car.SerializeACarArray(arrayPath,company);
            Car arrayDeserialization = new Car(arrayPath); 

            //adding bew car to a file
            //Car mclaren = new Car("GT","Mclaren",2021,"Green",1,2); before changing
            Car mclaren = new Car("GT","Mclaren",1990,"Green",1,2);
            //Car.SerializeACar(path, mclaren);
            Console.WriteLine(Car.DeserializeACar(path));
            Console.WriteLine(mclaren.CarComare(path)); // Before changing data --> return true
                                                        // After changing tear to 1990 --> return false
        }

        
    }
}
