﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Lesson21_ChallengePart
{
    [XmlRoot(ElementName ="ArrayOfCar")] // i use this case i had an error in xml document (2,2)
    public class Car
    {
        public string Model { get; set; }
        public string Brand { get; set; }

        
        public int Year { get; set; }
        public string Color { get; set; }
        private int codan;
        protected int numberOfSeats;

        public int GetCodan()
        {
            return this.codan;
        }

        public int GetNumberOfSeats()
        {
            return this.numberOfSeats;
        }

        protected void ChangeNumberOfSeats(int newNumberOfSeats)
        {
            if (numberOfSeats > 0)
            {
                this.numberOfSeats = newNumberOfSeats;
            }
        }
        public Car()
        {

        }
        public Car(string model, string brand, int year, string color, int codan, int seatNumber)
        {
            Model = model;
            Brand = brand;
            Year = year;
            Color = color;
            this.codan = codan;
            this.numberOfSeats = seatNumber;
        }

        public Car(string filename)
        {
            Car car = Car.DeserializeACar(filename);

            this.Model = car.Model;
            this.Brand = car.Brand;
            this.Year = car.Year;
            this.Color = car.Color;
        }

        public static void SerializeACar(string filename, Car car)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Car));
            using (Stream file = new FileStream(filename, FileMode.Create))
            {
                xmlSerializer.Serialize(file, car);
            }
            Console.WriteLine("File has been serialized");
        }

        public static void SerializeACarArray(string filename, Car[] car)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Car[]));
            using (Stream file = new FileStream(filename, FileMode.Create))
            {
                xmlSerializer.Serialize(file, car);
            }
            Console.WriteLine("File has been serialized");
        }

        public static Car DeserializeACar(string filename)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Car));
            Car car = null;

            using (Stream reader = new FileStream(filename, FileMode.Open))
            {
                car = serializer.Deserialize(reader) as Car;
            }
            return car;
        }

        public static Car[] DeserializeACarArray(string filename)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Car));
            Car[] car = null;

            using (Stream reader = new FileStream(filename, FileMode.Open))
            {
                car = serializer.Deserialize(reader) as Car[];
            }
            return car;
        }

        public bool CarComare(string filename)
        {
            Car car = Car.DeserializeACar(filename);
            if (this.Model == car.Model && this.Brand == car.Brand && this.Year==car.Year && this.Color ==car.Color)
            {
                return true;
            }
            return false;
        }

        public override string ToString()
        {
            return $"{base.ToString()}    Model: {this.Model}   Brand: {this.Brand}   Year: {Year}   Color: {this.Color}";
        }
    }
}
