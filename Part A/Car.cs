﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Lesson21_HW
{
    [XmlRoot("Honda")]
    public class Car
    {
        public string Model { get; set; }
        public string Brand { get; set; }

        [XmlElement("Shana")]
        public int Year { get; set; }
        [XmlAttribute("Color")]
        public string Color { get; set; }
        private int codan;
        protected int numberOfSeats;

        public Car()
        {

        }
        public Car(string model, string brand, int year, string color, int codan, int seatNumber)
        {
            Model = model;
            Brand = brand;
            Year = year;
            Color = color;
            this.codan = codan;
            this.numberOfSeats = seatNumber;
        }

        public int GetCodan()
        {
            return this.codan;
        }
        
        public int GetNumberOfSeats()
        {
            return this.numberOfSeats;
        }

        protected void ChangeNumberOfSeats(int newNumberOfSeats)
        {
            if (numberOfSeats > 0)
            {
                this.numberOfSeats = newNumberOfSeats;
            }
        }

        public override string ToString()
        {
            return $"{base.ToString()}  Model: {this.Model}   Brand: {this.Brand}   Year: {Year}   Color: {this.Color}";
        }
    }
}
