﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Lesson21_HW
{
    class Program
    {
        static void Main(string[] args)
        {
            Car car = new Car("R8","Audi",2020,"Black",1, 2);
            
            XmlSerializer xml = new XmlSerializer(typeof(Car));
            ////creating new file stream
            //using (Stream file = new FileStream(@"..\..\car.xml", FileMode.Create)) 
            //{
            //    xml.Serialize(file, car);
            //}

            //Deserializtion
            Car car_from_file = null;
            using (Stream file = new FileStream(@"..\..\car.xml", FileMode.Open))
            {
                car_from_file = xml.Deserialize(file) as Car;
            }

            Car[] cars = new Car[5]
            {
                new Car("Urus","Lamborgini",2020,"Yellow",2,2),
                new Car("Cubra", "Seat", 2018, "Gray",5,5),
                new Car("Q8rs", "Audi", 2020, "Black",3,8),
                new Car("R", "VW", 2019, "Blue",1,5),
                new Car("Rang Rover Sport", "Land Rover", 2021, "Black",5,5),
            };
            
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Car[]));
            //Create new file stream
            using (Stream cars_file = new FileStream(@"..\..\carArray.xml", FileMode.Create))
            {
                xmlSerializer.Serialize(cars_file, cars);
            }

            //Deserializtion
            Car[] cars1 = null;
            using (Stream cars_file = new FileStream(@"..\..\carArray.xml", FileMode.Open))
            {
                cars1 = xmlSerializer.Deserialize(cars_file) as Car[];
            }
            Print(cars1);
        }
        static void Print(Car[] cars)
        {
            foreach (Car item in cars)
            {
                Console.WriteLine(item);
            }
        }

    }
}
